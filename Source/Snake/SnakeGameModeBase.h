// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameModeBase.generated.h"

class AFood;
class UPauseWidget;
class USoundCue;

/**
 * 
 */
UCLASS()
class SNAKE_API ASnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPauseWidget* PauseWidget;

public:
	ASnakeGameModeBase();

	void OnFoodCollected();

	UPROPERTY(EditAnywhere)
		float SpawnRadius;

	UPROPERTY(EditAnywhere)
		int32 PointsToWin;

	UPROPERTY(EditAnywhere)
		float TimeToWin;

	float PlayTime;

	FTimerHandle TimeToWinCountdownHandle;

	void TimerTick();
	void GameOver();

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		TSubclassOf<AFood> FoodClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AFood> FoodSlowClass;

	UFUNCTION(BlueprintCallable)
		void SpawnFood();

	UFUNCTION(BlueprintCallable)
		void ResetGame();

	UFUNCTION(BlueprintCallable)
		void LevelCompleted();

	FTimerHandle LevelCompletedHandle;
	FTimerHandle BonusSpawnHandle;
	FTimerHandle RestartGameHandle;

	FTimerHandle StartCountdownHandle;
	void StartCountdown();
	bool bCanSnakeMove;
	UPROPERTY(EditAnywhere)
		int32 StartLevelCountdown;

	UFUNCTION(BlueprintCallable)
		void PauseGame();

	UFUNCTION(BlueprintCallable)
		void UnPauseGame();

	UPROPERTY(EditAnywhere, Category = "Widgets")
		TSubclassOf<UUserWidget> PauseWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Sound)
		USoundCue* GameoverCue;
};
