// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "Runtime/UMG/Public/UMG.h"

#include "ScoreWidget.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_API UScoreWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UScoreWidget(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;

	void UpdateScore(int32 Value);
	void UpdateTimerCountdown(float Value);
	void UpdateStartTimerCountdown(float Value);
	void UpdateLevelName(FString Value);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* TXTScore;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* TXTCountdown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* TXTStartCountdown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* TXTLevelName;
};
