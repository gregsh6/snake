// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeGameModeBase.h"
#include "MenuHUD.h"
#include "SnakePlayerController.h"
#include "GameHUD.h"
//#include "SnakeElementBase.h"
#include "Food.h"
#include "NavigationSystem.h"
#include "PauseWidget.h"
#include "Sound/SoundCue.h"
#include "SnakeGameInstance.h"

ASnakeGameModeBase::ASnakeGameModeBase()
{
	PlayerControllerClass = ASnakePlayerController::StaticClass();
	HUDClass = AMenuHUD::StaticClass();

	PointsToWin = 10;
	TimeToWin = 5.f;
	PlayTime = 0.f;
	SpawnRadius = 50;

	bCanSnakeMove = false;
	StartLevelCountdown = 3;

	static ConstructorHelpers::FObjectFinder<USoundCue> GameoverCueObject(TEXT("SoundCue'/Game/Audio/Gameover_Cue.Gameover_Cue'"));
	if (GameoverCueObject.Succeeded())
	{
		GameoverCue = GameoverCueObject.Object;
	}
}

void ASnakeGameModeBase::BeginPlay()
{
	if (GetWorld()->GetName() != "MainMenu")
	{
		APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
		PlayerController->bShowMouseCursor = false;
		PlayerController->SetInputMode(FInputModeGameOnly());

		if (!GetWorld()->GetTimerManager().IsTimerActive(StartCountdownHandle))
		{
			GetWorld()->GetTimerManager().SetTimer(StartCountdownHandle, this, &ASnakeGameModeBase::StartCountdown, 1.f, true);
		}
		else
		{
			GetWorldTimerManager().ClearTimer(StartCountdownHandle);
			GetWorld()->GetTimerManager().SetTimer(StartCountdownHandle, this, &ASnakeGameModeBase::StartCountdown, 1.f, true);
		}
	}
}

void ASnakeGameModeBase::StartCountdown()
{
	APawn* PlayerPawn = GetWorld()->GetFirstPlayerController()->GetPawn();
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	AGameHUD* GameHUD = Cast<AGameHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());

	if (StartLevelCountdown <= 0)
	{
		GetWorldTimerManager().ClearTimer(StartCountdownHandle);

		PlayerPawn->EnableInput(PlayerController);
		GameHUD->UpdateLevelName("");
		GameHUD->UpdateStartTimerCountdown(0);
		bCanSnakeMove = true;

		if (!GetWorld()->GetTimerManager().IsTimerActive(TimeToWinCountdownHandle))
		{
			GetWorld()->GetTimerManager().SetTimer(TimeToWinCountdownHandle, this, &ASnakeGameModeBase::TimerTick, 1.f, true);
		}
		else
		{
			GetWorldTimerManager().ClearTimer(TimeToWinCountdownHandle);
			GetWorld()->GetTimerManager().SetTimer(TimeToWinCountdownHandle, this, &ASnakeGameModeBase::TimerTick, 1.f, true);
		}

		if (!GetWorld()->GetTimerManager().IsTimerActive(BonusSpawnHandle))
		{
			GetWorld()->GetTimerManager().SetTimer(BonusSpawnHandle, this, &ASnakeGameModeBase::SpawnFood, 10.f, true);
		}
		else
		{
			GetWorldTimerManager().ClearTimer(BonusSpawnHandle);
			GetWorld()->GetTimerManager().SetTimer(BonusSpawnHandle, this, &ASnakeGameModeBase::SpawnFood, 10.f, true);
		}
	}
	else
	{
		PlayerPawn->DisableInput(PlayerController);
		if (GameHUD)
		{
			GameHUD->UpdateLevelName(GetWorld()->GetName());
			GameHUD->UpdateStartTimerCountdown(StartLevelCountdown);
			StartLevelCountdown--;
		}
	}
}

void ASnakeGameModeBase::PauseGame()
{
	if (PauseWidgetClass)
	{
		PauseWidget = CreateWidget<UPauseWidget>(GetWorld(), PauseWidgetClass);
		if (PauseWidget)
		{
			PauseWidget->AddToViewport();

			APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
			PlayerController->bShowMouseCursor = true;
			PlayerController->SetInputMode(FInputModeUIOnly());

			UGameplayStatics::SetGamePaused(this, true);
			bCanSnakeMove = false;
		}
	}
}

void ASnakeGameModeBase::UnPauseGame()
{
	if (PauseWidget)
	{
		PauseWidget->RemoveFromParent();

		APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
		PlayerController->bShowMouseCursor = false;
		PlayerController->SetInputMode(FInputModeGameOnly());

		UGameplayStatics::SetGamePaused(this, false);
		bCanSnakeMove = true;
	}
}

void ASnakeGameModeBase::OnFoodCollected()
{
	AGameHUD* GameHUD = Cast<AGameHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
	if (GameHUD)
	{
		if (PointsToWin > 0)
		{
			PointsToWin--;
			GameHUD->UpdateScore(PointsToWin);
		}
	}
}

void ASnakeGameModeBase::TimerTick()
{
	AGameHUD* GameHUD = Cast<AGameHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
	if (GameHUD)
	{
		PlayTime++;
		GameHUD->UpdateTimerCountdown(TimeToWin - PlayTime);
		if (PlayTime >= TimeToWin)
		{
			ASnakeGameModeBase::GameOver();
		}
	}
}

void ASnakeGameModeBase::LevelCompleted()
{
	USnakeGameInstance* GI = Cast<USnakeGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (GI)
	{
		if (!GetWorld()->GetTimerManager().IsTimerActive(LevelCompletedHandle))
		{
			bCanSnakeMove = false;
			GetWorldTimerManager().ClearTimer(TimeToWinCountdownHandle);
			GetWorldTimerManager().ClearTimer(BonusSpawnHandle);

			AGameHUD* GameHUD = Cast<AGameHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());

			if (GetWorld()->GetName() == "Level01")
			{
				GameHUD->UpdateLevelName("Level completed! :)");
				GI->NextLevel = "Level02";
			}
			else if (GetWorld()->GetName() == "Level02")
			{
				GameHUD->UpdateLevelName("Level completed! :)");
				GI->NextLevel = "Level03";
			}
			else if (GetWorld()->GetName() == "Level03")
			{
				GameHUD->UpdateLevelName("Game completed! :)");
				GI->NextLevel = "MainMenu";
			}
			else
			{
				GameHUD->UpdateLevelName("Level completed! :)");
				GI->NextLevel = "Level02";
			}

			GetWorld()->GetTimerManager().SetTimer(LevelCompletedHandle, this, &ASnakeGameModeBase::LevelCompleted, 5.f, false);
		}
		else
		{
			UGameplayStatics::OpenLevel(this, GI->NextLevel);
		}
	}
}

void ASnakeGameModeBase::GameOver()
{
	UGameplayStatics::PlaySound2D(this, GameoverCue, 2.f, 1.f, 0.f);
	//USoundCue* GameoverCue;
	//UAudioComponent* GameoverAudioComponent;
	//static ConstructorHelpers::FObjectFinder<USoundCue> GameoverCueObject(TEXT("SoundCue'/Game/Audio/Food_Cue.Food_Cue'"));
	//if (GameoverCueObject.Succeeded())
	//{
	//	GameoverCue = GameoverCueObject.Object;
	//	GameoverAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("FoodAudioComponent"));
	//}
	/*UAudioComponent* AudioComponent = UGameplayStatics::SpawnSound2D(this, SoundCue, volume_multiplier);*/

	bCanSnakeMove = false;
	GetWorldTimerManager().ClearTimer(TimeToWinCountdownHandle);
	GetWorldTimerManager().ClearTimer(BonusSpawnHandle);

	AGameHUD* GameHUD = Cast<AGameHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
	GameHUD->UpdateLevelName("Game over :(");

	GetWorld()->GetTimerManager().SetTimer(RestartGameHandle, this, &ASnakeGameModeBase::ResetGame, 5.f, false);
}

void ASnakeGameModeBase::ResetGame()
{
	UGameplayStatics::OpenLevel(this, "MainMenu");
}

void ASnakeGameModeBase::Tick(float DeltaTime)
{

}

void ASnakeGameModeBase::SpawnFood()
{
	UNavigationSystemV1* NavigationArea = FNavigationSystem::GetCurrent<UNavigationSystemV1>(this);
	if (NavigationArea) {
		FRotator Rot = FRotator::ZeroRotator;
		FActorSpawnParameters Parameters;
		Parameters.Owner = this;
		//UGameplayStatics::GetPlayerPawn(GetWorld(), 0)->GetActorLocation()
		FVector startPosi = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
		FNavLocation endPosi = FNavLocation(startPosi);
		if (NavigationArea->GetRandomReachablePointInRadius(startPosi, SpawnRadius, endPosi)) {
			if (FMath::RandBool())
			{
				AFood* NewFoodElement = GetWorld()->SpawnActor<AFood>(FoodClass, endPosi.Location, Rot, Parameters);
			}
			else
			{
				AFood* NewFoodElement = GetWorld()->SpawnActor<AFood>(FoodSlowClass, endPosi.Location, Rot, Parameters);
			}
		}
	}

	/*TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASnakeElementBase::StaticClass(), FoundActors);
	ASnakeElementBase* SnakeHead = Cast<ASnakeElementBase>(FoundActors[0]);
	if (SnakeHead)
	{
		FVector Loc = SnakeHead->GetActorLocation();

		Loc.X += FMath::RandRange(100, SpawnRadius);
		Loc.Y += FMath::RandRange(100, SpawnRadius);
		Loc.Z = 45;

		FTransform NewTransform(Loc);

		AFood* NewFoodElement = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
	}*/
}