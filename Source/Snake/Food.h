// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Components/AudioComponent.h"

#include "Food.generated.h"

UENUM(BlueprintType)
enum class EFoodType : uint8
{
	E_HEALTHY	UMETA(DisplayName = "HEALTHY"),
	E_DEADLY	UMETA(DisplayName = "DEADLY"),
	E_FASTER	UMETA(DisplayName = "FASTER"),
	E_SLOWER	UMETA(DisplayName = "SLOWER"),
	E_ICE		UMETA(DisplayName = "ICE"),
	E_WALL		UMETA(DisplayName = "WALL"),
	E_ENDLEVEL	UMETA(DisplayName = "ENDLEVEL")
};

UCLASS()
class SNAKE_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(EditDefaultsOnly)
		EFoodType FoodType;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	virtual void InteractEnd(AActor* Interactor, bool bIsHead) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Sound)
	class USoundCue* FoodSoundCue;

private:
	UAudioComponent* FoodAudioComponent;
};
