// Fill out your copyright notice in the Description page of Project Settings.


#include "GameHUD.h"
#include "SnakeGameModeBase.h"

AGameHUD::AGameHUD()
{
}

void AGameHUD::BeginPlay()
{
	Super::BeginPlay();

	if (ScoreWidgetClass && GetWorld()->GetName() != "MainMenu")
	{
		ScoreWidget = CreateWidget<UScoreWidget>(GetWorld(), ScoreWidgetClass);
		if (ScoreWidget)
		{
			ScoreWidget->AddToViewport();

			ASnakeGameModeBase* GM = Cast<ASnakeGameModeBase>(GetWorld()->GetAuthGameMode());
			AGameHUD::UpdateScore(GM->PointsToWin);
			AGameHUD::UpdateTimerCountdown(GM->TimeToWin);
		}
	}
}

void AGameHUD::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void AGameHUD::DrawHUD()
{
	Super::DrawHUD();
}

void AGameHUD::UpdateScore(int32 Value)
{
	if (ScoreWidget)
	{
		ScoreWidget->UpdateScore(Value);
	}
}

void AGameHUD::UpdateTimerCountdown(float Value)
{
	if (ScoreWidget)
	{
		ScoreWidget->UpdateTimerCountdown(Value);
	}
}

void AGameHUD::UpdateLevelName(FString Value)
{
	if (ScoreWidget)
	{
		ScoreWidget->UpdateLevelName(Value);
	}
}

void AGameHUD::UpdateStartTimerCountdown(float Value)
{
	if (ScoreWidget)
	{
		ScoreWidget->UpdateStartTimerCountdown(Value);
	}
}
