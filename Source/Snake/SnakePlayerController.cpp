// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakePlayerController.h"
#include "MenuHUD.h"
#include "SnakeGameModeBase.h"

ASnakePlayerController::ASnakePlayerController()
{

}

void ASnakePlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	if (InputComponent)
	{
		InputComponent->BindAction("OpenMenu", IE_Pressed, this, &ASnakePlayerController::OpenMenu);
	}
}

void ASnakePlayerController::OpenMenu()
{
	if (ASnakeGameModeBase* GM = Cast<ASnakeGameModeBase>(GetWorld()->GetAuthGameMode()))
	{
		GM->PauseGame();
	}
	//if (AMenuHUD* MenuHUD = Cast<AMenuHUD>(GetHUD()))
	//{
	//	MenuHUD->ShowMenu();
	//}
}
