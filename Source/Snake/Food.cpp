// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeGameModeBase.h"
#include "Sound/SoundCue.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FObjectFinder<USoundCue> FoodSoundCueObject(TEXT("SoundCue'/Game/Audio/Food_Cue.Food_Cue'"));
	if (FoodSoundCueObject.Succeeded())
	{
		FoodSoundCue = FoodSoundCueObject.Object;
		FoodAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("FoodAudioComponent"));
		FoodAudioComponent->SetupAttachment(RootComponent);
	}
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
	if (FoodAudioComponent && FoodSoundCue)
	{
		FoodAudioComponent->SetSound(FoodSoundCue);
	}
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			ASnakeGameModeBase* GM = Cast<ASnakeGameModeBase>(GetWorld()->GetAuthGameMode());
			switch (FoodType)
			{
				case EFoodType::E_HEALTHY:
				{
					if (GM)
					{
						GM->OnFoodCollected();

						Snake->AddSnakeElement();
						this->Destroy();

						if (FoodAudioComponent)
						{
							FoodAudioComponent->Play(0.f);
						}
					}
					break;
				}
				case EFoodType::E_DEADLY: case EFoodType::E_WALL:
				{
					GM->GameOver();
					break;
				}
				case EFoodType::E_FASTER:
				{
					Snake->MovementSpeed -= Snake->MovementSpeed * 0.2f;
					Snake->SetActorTickInterval(Snake->MovementSpeed);
					this->Destroy();

					if (FoodAudioComponent)
					{
						FoodAudioComponent->Play(0.f);
					}
					break;
				}
				case EFoodType::E_SLOWER:
				{
					Snake->MovementSpeed += Snake->MovementSpeed * 0.2f;
					Snake->SetActorTickInterval(Snake->MovementSpeed);
					this->Destroy();

					if (FoodAudioComponent)
					{
						FoodAudioComponent->Play(0.f);
					}
					break;
				}
				case EFoodType::E_ICE:
				{
					APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
					APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
					PlayerPawn->DisableInput(PlayerController);
					break;
				}
				case EFoodType::E_ENDLEVEL:
				{
					if (GM)
					{
						if (GM->PointsToWin <= 0)
						{
							if (FoodAudioComponent)
							{
								FoodAudioComponent->Play(0.f);
							}
							GM->LevelCompleted();
						}
						else
						{
							GM->GameOver();
						}
					}
					break;
				}
			}
		}
	}
}

void AFood::InteractEnd(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			if (FoodType == EFoodType::E_ICE)
			{
				APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
				APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
				PlayerPawn->EnableInput(PlayerController);
			}
		}
	}
}
