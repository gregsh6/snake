// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"

#include "Components/WidgetComponent.h"

#include "ScoreWidget.h"

#include "GameHUD.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_API AGameHUD : public AHUD
{
	GENERATED_BODY()
public:
	AGameHUD();

	virtual void DrawHUD() override;

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION()
	void UpdateScore(int32 Value);

	UFUNCTION()
	void UpdateTimerCountdown(float Value);

	UFUNCTION()
	void UpdateLevelName(FString Value);

	UFUNCTION()
	void UpdateStartTimerCountdown(float Value);

	UPROPERTY(EditAnywhere, Category = "Widgets")
	TSubclassOf<UUserWidget> ScoreWidgetClass;
private:
	UScoreWidget* ScoreWidget;
};
