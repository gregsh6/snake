// Fill out your copyright notice in the Description page of Project Settings.


#include "ScoreWidget.h"

UScoreWidget::UScoreWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

void UScoreWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void UScoreWidget::UpdateScore(int32 Value)
{
	if (TXTScore)
	{
		if (Value > 0)
		{
			TXTScore->SetText(FText::FromString("Need to collect: " + FString::FromInt(Value)));
		}
		else
		{
			TXTScore->SetText(FText::FromString("You can exit this level!"));
		}
	}
}

void UScoreWidget::UpdateTimerCountdown(float Value)
{
	if (TXTCountdown)
	{
		TXTCountdown->SetText(FText::FromString("Time left: " + FString::FromInt(static_cast<int32>(Value))));
	}
}

void UScoreWidget::UpdateStartTimerCountdown(float Value)
{
	if (TXTStartCountdown)
	{
		if (Value > 0)
		{
			TXTStartCountdown->SetText(FText::FromString(FString::FromInt(static_cast<int32>(Value))));
		}
		else
		{
			TXTStartCountdown->SetText(FText::FromString(""));
		}
	}
}

void UScoreWidget::UpdateLevelName(FString Value)
{
	if (TXTLevelName)
	{
		TXTLevelName->SetText(FText::FromString(Value));
	}
}
