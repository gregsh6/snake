// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "SnakeGameModeBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::LEFT;
	WantedMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ASnakeGameModeBase* GM = Cast<ASnakeGameModeBase>(GetWorld()->GetAuthGameMode());
	if (GM->bCanSnakeMove)
	{
		Move();
	}
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i) {
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 45);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		int32 ElementIndex = SnakeElements.Add(NewSnakeElement);
		if (ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
		else
		{
			NewSnakeElement->GetRootComponent()->ToggleVisibility();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	float HeadRotation = 0;
	switch (WantedMoveDirection)
	{
	case EMovementDirection::UP:
		if (LastMoveDirection != EMovementDirection::DOWN)
		{
			MovementVector.X += ElementSize;
			LastMoveDirection = EMovementDirection::UP;
			HeadRotation = 0;
		}
		else
		{
			MovementVector.X -= ElementSize;
		}
		break;
	case EMovementDirection::DOWN:
		if (LastMoveDirection != EMovementDirection::UP)
		{
			MovementVector.X -= ElementSize;
			LastMoveDirection = EMovementDirection::DOWN;
			HeadRotation = -180;
		}
		else
		{
			MovementVector.X += ElementSize;
		}
		break;
	case EMovementDirection::LEFT:
		if (LastMoveDirection != EMovementDirection::RIGHT)
		{
			MovementVector.Y -= ElementSize;
			LastMoveDirection = EMovementDirection::LEFT;
			HeadRotation = -90;
		}
		else
		{
			MovementVector.Y += ElementSize;
		}
		break;
	case EMovementDirection::RIGHT:
		if (LastMoveDirection != EMovementDirection::LEFT)
		{
			MovementVector.Y += ElementSize;
			LastMoveDirection = EMovementDirection::RIGHT;
			HeadRotation = 90;
		}
		else
		{
			MovementVector.Y -= ElementSize;
		}
		break;
	}

	SnakeElements[0]->ToggleCollision();
	SnakeElements[0]->GetRootComponent()->SetRelativeRotation(FRotator(0.f, HeadRotation, 0.f));

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i-1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);

		if (!CurrentElement->GetRootComponent()->IsVisible())
		{
			CurrentElement->GetRootComponent()->ToggleVisibility();
		}
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}

	}
}

void ASnakeBase::SnakeElementOverlapEnd(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->InteractEnd(this, bIsFirst);
		}

	}
}