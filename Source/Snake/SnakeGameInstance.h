// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "SnakeGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_API USnakeGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	USnakeGameInstance();
	FName NextLevel;
};
